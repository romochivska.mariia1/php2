<?php


$gasStation = [
    'code' => null,
    'address' => null,
    'firm' => null,
    'price' => null,
    'fuel' => null,
    'litre' => null,

];

$stations = [
    [


        'code' => 1,
        'address' => 'Italy',
        'firm'=> 'SHELL',
        'price' => 61,
        'fuel' => 'dp',
        'litre' => 300,
    ],
    [
        'code' => 2,
        'address' => 'Germany',
        'firm'=> 'GLUSCO',
        'price' => 47,
        'fuel' => 'a-98',
        'litre' => 500,
    ],

    [
        'code' => 3,
        'address' => 'France',
        'firm'=> 'UPG',
        'price' => 45,
        'fuel' => 'dp',
        'litre' => 900,
    ],

    [
        'code' => 4,
        'address' => 'Spain',
        'firm'=> 'WOG',
        'price' => 42,
        'fuel' => 'a-92',
        'litre' => 250,
    ],
    [
        'code' => 5,
        'address' => 'Spain',
        'firm'=> 'SHELL',
        'price' => 49,
        'fuel' => 'a-98',
        'litre' => 250,
    ],
    [
        'code' => 6,
        'address' => 'Slovakia',
        'firm'=> 'Socar',
        'price' => 45,
        'fuel' => 'a-95',
        'litre' => 250,
    ],
];

include 'table.phtml';

$stations = [];

function saveAndSerializeData($data)
{
    $json_data = json_encode($data, JSON_PRETTY_PRINT);

    if (file_put_contents('data.json', $json_data) !== false) {
        return true;
    } else {
        return false;
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['save'])) {
        $address = $_POST['address'];
        $firm = $_POST['firm'];
        $litre = $_POST['litre'];

        $new_entry = [
            'address' => $address,
            'firm' => $firm,
            'litre' => $litre,
        ];

        $stations[] = $new_entry;

        if (saveAndSerializeData($stations)) {
            echo 'Дані збережено успішно.';
        } else {
            echo 'Помилка під час збереження даних.';
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Форма для збереження даних</title>
</head>
<body>
<h1>Форма для збереження даних</h1>
<form method="POST" action="">
    <label for="address">Адреса:</label>
    <input type="text" id="address" name="address" placeholder="Введіть адресу" required><br>

    <label for="litre">Літраж:</label>
    <input type="text" id="litre" name="litre" placeholder="Введіть літраж" required><br>

    <label for="firm">Фірма:</label>
    <input type="text" id="firm" name="firm" placeholder="Введіть фірму" required><br>

    <input type="submit" name="save" value="Зберегти дані">
</form>
</body>
</html>



